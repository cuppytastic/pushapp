﻿using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        String currentDevice = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            var push = new PushBroker();

            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

            String str = "server=localhost;database=GardaMobileCMS;UID=sa;password=sqlL0ck0ut";
            String query = "select devices.DeviceToken, devices.AppID, Devices.DeviceType, Message.MessageHeader, " +
            "Message.MessageBody, outbox.Status from Outbox join Devices on outbox.devicetoken = devices.devicetoken " +
            "join Message on Outbox.MessageID = Message.MessageID";
            con = new SqlConnection(str);
            cmd = new SqlCommand(query, con);
            con.Open();

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    currentDevice = reader["DeviceToken"].ToString();
                    if (reader["DeviceType"].ToString() == "1" && reader["AppID"].ToString() == "2") //iOS-otocare
                    {
                        //var certificate = new X509Certificate2(System.IO.File.ReadAllBytes("com.aab.Otocare.p12"), "Otocare123##", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                        var certificate = new X509Certificate2(System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["OtocareP12Path"]), ConfigurationManager.AppSettings["OtocareKeyPassword"], X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                        //"f663d0c1426732eb81afacfdaf5da84d1ac30a23aa71c935b55fea1ed2096b23" device id marko
                        push.RegisterAppleService(new ApplePushChannelSettings(false, certificate, false));
                        push.QueueNotification(new AppleNotification()
                                                .ForDeviceToken(reader["DeviceToken"].ToString())//the recipient device id
                                                .WithAlert(reader["MessageHeader"].ToString())//the message
                                                .WithBadge(1)
                                                .WithSound("sound.caf")
                                                );
                    }

                    else if (reader["DeviceType"].ToString() == "0" && reader["AppID"].ToString() == "2") //Android-otocare
                    {
                        push.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["OtocareGCMSenderAuthToken"]));
                        push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(reader["DeviceToken"].ToString())
                                            .WithJson("{\"MSG\":\"" + reader["MessageHeader"].ToString() + "\",\"KEY\":\"1125\"}"));
                        //APA91bFUILX8kzU6xIn9k8N4MAL5QjOQTRT7u-zbK5RkqkCAP8LBQbIR3ouFegBfaXnVE3a8vD2rWSfR_oGabXXnocAlWBLWrCrzE1nVqhf2C60HpNBFQet8lIcDm00VhzgmIB-6FvtmcPoRRbFNZ2MSVh-CIu1-AekCVIAU_qis5MgFgSWOfdI
                    }

                    else if (reader["DeviceType"].ToString() == "1" && reader["AppID"].ToString() == "1") //iOS-medcare
                    {
                        var certificate = new X509Certificate2(System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["MedcareP12Path"]), ConfigurationManager.AppSettings["MedcareKeyPassword"], X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                        //"f663d0c1426732eb81afacfdaf5da84d1ac30a23aa71c935b55fea1ed2096b23" device id marko
                        push.RegisterAppleService(new ApplePushChannelSettings(false, certificate, false));
                        push.QueueNotification(new AppleNotification()
                                                .ForDeviceToken(reader["DeviceToken"].ToString())//the recipient device id
                                                .WithAlert(reader["MessageHeader"].ToString())//the message
                                                .WithBadge(1)
                                                .WithSound("sound.caf")
                                                );
                    }

                    else if (reader["DeviceType"].ToString() == "0" && reader["AppID"].ToString() == "1") //Android-medcare
                    {
                        push.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["MedcareGCMSenderAuthToken"]));
                        push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(reader["DeviceToken"].ToString())
                                            .WithJson("{\"MSG\":\"" + reader["MessageHeader"].ToString() + "\",\"KEY\":\"1125\"}"));
                        //APA91bFUILX8kzU6xIn9k8N4MAL5QjOQTRT7u-zbK5RkqkCAP8LBQbIR3ouFegBfaXnVE3a8vD2rWSfR_oGabXXnocAlWBLWrCrzE1nVqhf2C60HpNBFQet8lIcDm00VhzgmIB-6FvtmcPoRRbFNZ2MSVh-CIu1-AekCVIAU_qis5MgFgSWOfdI
                    }
                }
            }

            con.Close();
            push.StopAllServices();
        }

        void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Currently this event will only ever happen for Android GCM
            Console.WriteLine("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification);
        }

        void NotificationSent(object sender, INotification notification)
        {
            Console.WriteLine("Sent: " + sender + " -> " + notification);
        }

        void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            Console.WriteLine("Failure: " + sender + " -> " + notificationFailureException.Message + " -> " + notification);
        }

        void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        void ServiceException(object sender, Exception exception)
        {
            Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            Console.WriteLine("Device Subscription Expired: " + sender + " -> " + expiredDeviceSubscriptionId);
        }

        void ChannelDestroyed(object sender)
        {
            Console.WriteLine("Channel Destroyed for: " + sender);
        }

        void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //con.Open();
            //MessageBox.Show("Channel Created for: " + sender);
            //String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Channel created for " + sender + "', '" + DateTime.Now + "')";
            //cmd = new SqlCommand(query, con);
            //cmd.ExecuteNonQuery();
        }
    }
}
