﻿using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace GardaMobilePush
{
    class Program
    {
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader reader;
        String currentDevice = "";
        static void Main(string[] args)
        {
            Program p = new Program();
            p.SendNotification();
            
            //Console.ReadLine();
        }

        void SendNotification()
        {
            var push = new PushBroker();
            var push1 = new PushBroker();
            var push2 = new PushBroker();
            var push3 = new PushBroker();
            var push4 = new PushBroker();

            bool pushRegistered = false;
            bool push1Registered = false;
            bool push2Registered = false;
            bool push3Registered = false;
            bool push4Registered = false;

            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

            //Wire up the events for all the services that the broker registers
            push1.OnNotificationSent += NotificationSent;
            push1.OnChannelException += ChannelException;
            push1.OnServiceException += ServiceException;
            push1.OnNotificationFailed += NotificationFailed;
            push1.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push1.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push1.OnChannelCreated += ChannelCreated;
            push1.OnChannelDestroyed += ChannelDestroyed;

            //Wire up the events for all the services that the broker registers
            push2.OnNotificationSent += NotificationSent;
            push2.OnChannelException += ChannelException;
            push2.OnServiceException += ServiceException;
            push2.OnNotificationFailed += NotificationFailed;
            push2.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push2.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push2.OnChannelCreated += ChannelCreated;
            push2.OnChannelDestroyed += ChannelDestroyed;

            //Wire up the events for all the services that the broker registers
            push3.OnNotificationSent += NotificationSent;
            push3.OnChannelException += ChannelException;
            push3.OnServiceException += ServiceException;
            push3.OnNotificationFailed += NotificationFailed;
            push3.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push3.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push3.OnChannelCreated += ChannelCreated;
            push3.OnChannelDestroyed += ChannelDestroyed;

            //Wire up the events for all the services that the broker registers
            push4.OnNotificationSent += NotificationSent;
            push4.OnChannelException += ChannelException;
            push4.OnServiceException += ServiceException;
            push4.OnNotificationFailed += NotificationFailed;
            push4.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push4.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push4.OnChannelCreated += ChannelCreated;
            push4.OnChannelDestroyed += ChannelDestroyed;

            String str = System.Configuration.ConfigurationManager.ConnectionStrings["GardaMobileCMSDB"].ConnectionString;
            //String query = "select devices.DeviceToken, devices.AppID, Devices.DeviceType, Message.MessageHeader, " +
            //"Message.MessageBody, outbox.Status, Message.MessageID from Outbox join Devices on outbox.devicetoken = devices.devicetoken " +
            //"join Message on Outbox.MessageID = Message.MessageID";
            String query = "sp_getMessageToSend";
            con = new SqlConnection(str);
            cmd = new SqlCommand(query, con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            con.Open();
            
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    try
                    {
                        currentDevice = reader["DeviceToken"].ToString();
                        if (reader["DeviceType"].ToString() == "1" && reader["AppID"].ToString() == "2") //iOS-otocare
                        {
                            //var certificate = new X509Certificate2(System.IO.File.ReadAllBytes("com.aab.Otocare.p12"), "Otocare123##", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                            var certificate = new X509Certificate2(System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["OtocareP12Path"]), ConfigurationManager.AppSettings["OtocareKeyPassword"], X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                            //"f663d0c1426732eb81afacfdaf5da84d1ac30a23aa71c935b55fea1ed2096b23" device id marko
                            if (!pushRegistered)
                            {
                                push.RegisterAppleService(new ApplePushChannelSettings(bool.Parse(ConfigurationManager.AppSettings["IsProduction"]), certificate, false));
                                pushRegistered = true;
                            }
                            push.QueueNotification(new AppleNotification()
                                                    .ForDeviceToken(reader["DeviceToken"].ToString())//the recipient device id
                                                    .WithAlert(reader["MessageHeader"].ToString())//the message
                                                    .WithSound("default")
                                                    .WithContentAvailable(1)
                                                    .WithCustomItem("MessageID", reader["MessageID"].ToString())
                                                    );
                        }

                        else if (reader["DeviceType"].ToString() == "0" && reader["AppID"].ToString() == "2") //Android-otocare
                        {
                            if(!push1Registered)
                            {
                                push1.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["OtocareGCMSenderAuthToken"]));
                                push1Registered = true;
                            }
                            push1.QueueNotification(new GcmNotification().ForDeviceRegistrationId(reader["DeviceToken"].ToString())
                                                .WithJson("{\"MSG\":\"" + reader["MessageHeader"].ToString() + "\",\"MSGID\":\"" + reader["MessageID"].ToString() + "\"}"));
                            
                        }

                        else if (reader["DeviceType"].ToString() == "1" && reader["AppID"].ToString() == "1") //iOS-medcare
                        {
                            var certificate = new X509Certificate2(System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["MedcareP12Path"]), ConfigurationManager.AppSettings["MedcareKeyPassword"], X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                            //"f663d0c1426732eb81afacfdaf5da84d1ac30a23aa71c935b55fea1ed2096b23" device id marko
                            if (!push2Registered)
                            {
                                push2.RegisterAppleService(new ApplePushChannelSettings(bool.Parse(ConfigurationManager.AppSettings["IsProduction"]), certificate, false));
                                push2Registered = true;
                            }
                            push2.QueueNotification(new AppleNotification()
                                                    .ForDeviceToken(reader["DeviceToken"].ToString())//the recipient device id
                                                    .WithAlert(reader["MessageHeader"].ToString())//the message
                                                    .WithSound("default")
                                                    .WithContentAvailable(1)
                                                    .WithCustomItem("MessageID", reader["MessageID"].ToString())
                                                    );
                        }

                        else if (reader["DeviceType"].ToString() == "0" && reader["AppID"].ToString() == "1") //Android-medcare
                        {
                            if (!push3Registered)
                            {
                                push3.RegisterGcmService(new GcmPushChannelSettings(ConfigurationManager.AppSettings["MedcareGCMSenderAuthToken"]));
                                push3Registered = true;
                            }
                            push3.QueueNotification(new GcmNotification().ForDeviceRegistrationId(reader["DeviceToken"].ToString())
                                                .WithJson("{\"MSG\":\"" + reader["MessageHeader"].ToString() + "\",\"MSGID\":\"" + reader["MessageID"].ToString() + "\"}"));
                            //APA91bFUILX8kzU6xIn9k8N4MAL5QjOQTRT7u-zbK5RkqkCAP8LBQbIR3ouFegBfaXnVE3a8vD2rWSfR_oGabXXnocAlWBLWrCrzE1nVqhf2C60HpNBFQet8lIcDm00VhzgmIB-6FvtmcPoRRbFNZ2MSVh-CIu1-AekCVIAU_qis5MgFgSWOfdI
                        }

                        else if (reader["DeviceType"].ToString() == "1" && reader["AppID"].ToString() == "3") //IOS-RAssistance
                        {

                            var certificate = new X509Certificate2(System.IO.File.ReadAllBytes(ConfigurationManager.AppSettings["RAssistanceP12Path"]), ConfigurationManager.AppSettings["RAssistanceKeyPassword"], X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
                            //"f663d0c1426732eb81afacfdaf5da84d1ac30a23aa71c935b55fea1ed2096b23" device id marko
                            if (!push4Registered)
                            {
                                push4.RegisterAppleService(new ApplePushChannelSettings(bool.Parse(ConfigurationManager.AppSettings["IsProduction"]), certificate, false));
                                push4Registered = true;
                            }
                            push4.QueueNotification(new AppleNotification()
                                                    .ForDeviceToken(reader["DeviceToken"].ToString())//the recipient device id
                                                    .WithAlert(reader["MessageHeader"].ToString())//the message
                                                    .WithSound("default")
                                                    .WithContentAvailable(1)
                                                    .WithCustomItem("MessageID", reader["MessageID"].ToString())
                                                    );
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }

            reader.Close();
            con.Close();
            push.StopAllServices();
            push1.StopAllServices();
            push2.StopAllServices();
            push3.StopAllServices();
            push1Registered = push2Registered = push3Registered = pushRegistered = false;
        }

        void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Currently this event will only ever happen for Android GCM
            Console.WriteLine("Device Registration Changed:  Old-> " + oldSubscriptionId + "  New-> " + newSubscriptionId + " -> " + notification);
        }

        void NotificationSent(object sender, INotification notification)
        {
           try
            {
                Console.WriteLine("Sent: " + sender + " -> " + notification);
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();
                String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Sent: " + sender + " - " + notification + "', GETDATE())";
                //String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Sent to Test', '" + DateTime.Now + "')";
                cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();

                string text = "Sent: " + sender + " -> " + notification + " Timestamp: " + DateTime.Now.ToString();
                using (StreamWriter sw = File.AppendText(@"C:\log.txt"))
                {
                    sw.WriteLine(text);
                    sw.WriteLine("");
                }
            }
            catch (Exception exc)
            {
                if (!File.Exists(@"C:\log.txt"))
                {
                    // Create a file to write to. 
                    using (StreamWriter sw = File.CreateText(@"C:\log.txt"))
                    {
                        sw.WriteLine("Log Created");
                    }
                }
                using (StreamWriter sw = File.AppendText(@"C:\log.txt"))
                {
                    sw.WriteLine(exc.ToString());
                    sw.WriteLine("");
                }
            }
            
        }

        void NotificationFailed(object sender, INotification notification, Exception notificationFailureException)
        {
            try
            {
                Console.WriteLine("Failed: " + sender + " -> " + notification + "Exception : " + notificationFailureException.ToString());
                if (con.State == System.Data.ConnectionState.Closed)
                    con.Open();
                String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Failed: " + sender + " - on exception " + notificationFailureException.ToString() + "', GETDATE())";
                //String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Sent to Test', '" + DateTime.Now + "')";
                cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();

                string text = "Failed: " + sender + " -> " + notification + " Timestamp: " + DateTime.Now.ToString();
                using (StreamWriter sw = File.AppendText(@"C:\log.txt"))
                {
                    sw.WriteLine(text);
                    sw.WriteLine("");
                }
            }
            catch (Exception exc)
            {
                if (!File.Exists(@"C:\log.txt"))
                {
                    // Create a file to write to. 
                    using (StreamWriter sw = File.CreateText(@"C:\log.txt"))
                    {
                        sw.WriteLine("Log Created");
                    }
                }
                using (StreamWriter sw = File.AppendText(@"C:\log.txt"))
                {
                    sw.WriteLine(exc.ToString());
                    sw.WriteLine("");
                }
            }
        }

        void ChannelException(object sender, IPushChannel channel, Exception exception)
        {
            Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        void ServiceException(object sender, Exception exception)
        {
            Console.WriteLine("Channel Exception: " + sender + " -> " + exception);
        }

        void DeviceSubscriptionExpired(object sender, string expiredDeviceSubscriptionId, DateTime timestamp, INotification notification)
        {
            Console.WriteLine("Device Subscription Expired: " + sender + " -> " + expiredDeviceSubscriptionId);
        }

        void ChannelDestroyed(object sender)
        {
            Console.WriteLine("Channel Destroyed for: " + sender);
        }

        void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //con.Open();
            //MessageBox.Show("Channel Created for: " + sender);
            //String query = "Insert into MessageLog (LogDetail, CreatedDate) values ('Channel created for " + sender + "', '" + DateTime.Now + "')";
            //cmd = new SqlCommand(query, con);
            //cmd.ExecuteNonQuery();
        }

    }
}
